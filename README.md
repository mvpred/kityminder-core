基于github的[kityminder-core](https://github.com/fex-team/kityminder-core) 进行自己定制化的二次开发
更多详细的开发资料可以参考 [wiki](https://github.com/fex-team/kityminder-core/wiki)

自定义参数 
方向 direction top|down|left|right
类型 nodeType 1-根节点 2-带边框的节点 3.不带边框的普通节点 4-功能节点（带图标FU） 5-失效模式 6-失效原因 7-失效影响 8-预防措施 9.探测措施 10.外部接口 11 内部接口 12 外部部件 13 优化后PC 14 优化后DC

    /**
     * 节点方向
     */
    public enum Direction {
        top, down, left, right;
    }

    public enum NodeType {
        root(1), // 根节点
        borderNode(2), // 带边框的节点
        noBorderNode(3), // 不带边框的节点
        function(4), // 功能
        failure(5), // 失效
        failureMode(6), // 失效模式
        failureReason(7), // 失效原因
        failureEffect(8), // 失效影响
        prevent(9), // 预防措施
        detection(10), // 探测措施
        outside(11), // 外部接口
        inside(12), // 内部接口
        outBom(13),// 外部部件
        optimizationPrevent(14),// 优化预防措施
        optimizationDetection(15);// 优化探测措施
        private int value;

        NodeType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }


开发说明
安装 Node版本 8.12.0(高版本构建可能报错)
安装 grunt
构建 npm install

启动 npm run dev
打包 npm build