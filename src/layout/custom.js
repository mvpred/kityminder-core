define(function (require, exports, module) {
    var kity = require('../core/kity');
    var Layout = require('../core/layout');
    var Minder = require('../core/minder');

    Layout.register('custom', kity.createClass({
        base: Layout,

        doLayout: function (node, children) {
            var left = [], right = [], down = [], top = [];


            children.forEach(function (child) {
                if ("down" === child.data.direction) {
                    down.push(child);
                } else if("top" === child.data.direction){
                    top.push(child);
                } else if("left" === child.data.direction){
                    left.push(child);
                } else {
                    right.push(child);
                }
            });

            var downLayout = Minder.getLayoutInstance("filetree-down");
            downLayout.doLayout(node, down);

            var topLayout = Minder.getLayoutInstance('filetree-up');
            topLayout.doLayout(node, top);

            var leftLayout = Minder.getLayoutInstance('left');
            leftLayout.doLayout(node, left);

            var rightLayout = Minder.getLayoutInstance('right');
            rightLayout.doLayout(node, right);
            
            var box = node.getContentBox();

            if ("root" === node.type) {
                node.setVertexOut(new kity.Point(box.left + 28, box.cy - 10));
            } else if ("main" === node.type) {
                node.setVertexOut(new kity.Point(box.left + 28, box.cy - 4));
            } else {
                node.setVertexOut(new kity.Point(box.left + 28, box.bottom));
            }
            
            node.setLayoutVectorOut(new kity.Vector(0, 0));
        },

        getOrderHint: function (node) {
            var hint = [];
            var box = node.getLayoutBox();
            var offset = 5;

            hint.push({
                type: 'up',
                node: node,
                area: new kity.Box({
                    x: box.x,
                    y: box.top - node.getStyle('margin-top') - offset,
                    width: box.width,
                    height: node.getStyle('margin-top')
                }),
                path: ['M', box.x, box.top - offset, 'L', box.right, box.top - offset]
            });

            hint.push({
                type: 'down',
                node: node,
                area: new kity.Box({
                    x: box.x,
                    y: box.bottom + offset,
                    width: box.width,
                    height: node.getStyle('margin-bottom')
                }),
                path: ['M', box.x, box.bottom + offset, 'L', box.right, box.bottom + offset]
            });
            return hint;
        }
    }));
});