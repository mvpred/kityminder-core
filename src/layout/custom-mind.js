define(function(require, exports, module) {
    var kity = require('../core/kity');
    var Layout = require('../core/layout');
    var Minder = require('../core/minder');

    Layout.register('custom-mind', kity.createClass({
        base: Layout,

        doLayout: function(node, children) {
            var layout = this;
            var half = Math.ceil(node.children.length / 2);
            var right = [];
            var left = [];
            var down = [];

            children.forEach(function(child) {
                if ( "right" == child.data.direction){
                    right.push(child);
                } else if("down" == child.data.direction){
                    down.push(child);
                }else {
                    left.push(child);
                }
            });

            var leftLayout = Minder.getLayoutInstance('left');
            leftLayout.doLayout(node, left);

            var rightLayout = Minder.getLayoutInstance('right');
            rightLayout.doLayout(node, right);

            var downLayout = Minder.getLayoutInstance("filetree-down");
            downLayout.doLayout(node, down);

            var box = node.getContentBox();
            if ("root" === node.type) {
                node.setVertexOut(new kity.Point(box.left + 28, box.cy - 10));
            } else if ("main" === node.type) {
                node.setVertexOut(new kity.Point(box.left + 28, box.cy - 10));
            } else {
                node.setVertexOut(new kity.Point(box.left + 28, box.bottom))
            }
            node.setLayoutVectorOut(new kity.Vector(0, 0));
        },

        getOrderHint: function(node) {
            var hint = [];
            var box = node.getLayoutBox();
            var offset = 5;

            hint.push({
                type: 'up',
                node: node,
                area: new kity.Box({
                    x: box.x,
                    y: box.top - node.getStyle('margin-top') - offset,
                    width: box.width,
                    height: node.getStyle('margin-top')
                }),
                path: ['M', box.x, box.top - offset, 'L', box.right, box.top - offset]
            });

            hint.push({
                type: 'down',
                node: node,
                area: new kity.Box({
                    x: box.x,
                    y: box.bottom + offset,
                    width: box.width,
                    height: node.getStyle('margin-bottom')
                }),
                path: ['M', box.x, box.bottom + offset, 'L', box.right, box.bottom + offset]
            });
            return hint;
        }
    }));
});