/**
 * @fileOverview
 *
 * 左右布局
 *
 */
define(function(require, exports, module) {
    var template = require('../core/template');

    template.register('custom-mind', {

        getLayout: function(node) {
            return 'custom-mind';
        },

        getConnect: function(node) {
            return "poly"
        }
    });
});