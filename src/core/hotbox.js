define(function (require, exports, module) {
    var kity = require('./kity');
    var Minder = require('./minder');

    var protocols = {};

    function registerProtocol(name, protocol) {
        protocols[name] = protocol;

        for (var pname in protocols) {
            if (protocols.hasOwnProperty(pname)) {
                protocols[pname] = protocols[pname];
                protocols[pname].name = pname;
            }
        }
    }

    function getRegisterProtocol(name) {
        return name === undefined ? protocols : (protocols[name] || null);
    }

    exports.registerProtocol = registerProtocol;
    exports.getRegisterProtocol = getRegisterProtocol;

    // 右键菜单 配合hotbox.js使用
    kity.extendClass(Minder, {

        /**
         * @method initHotbox()
         * @for Minder
         * @description
         *     初始化右键菜单
         */
        initHotbox: function (ringButtons) {
            var renderTo = this._options.renderTo;
            var minder = this;
            var hotbox = new HotBox(renderTo);
            hotbox.control();

            var main = hotbox.state('main');

            ringButtons.forEach(function (item) {
                main.button({
                    position: item.position || "ring",
                    label: item.label,
                    enable: item.enable,
                    key: item.key,
                    action: item.action
                });
            });

            var container = document.getElementById(renderTo.replace("#", ""));
            var downX, downY;
            var MOUSE_RB = 2;

            // 鼠标按下事件
            container.addEventListener("mousedown", function (e) {
                // 关闭右键菜单
                // destroyRightMenu();
                hotbox.active("idle");

                // 右键
                if (e.button == MOUSE_RB) {
                    // destroyRightMenu();
                    hotbox.active("idle");
                    e.preventDefault();

                    // 记录xy轴
                    downX = e.clientX;
                    downY = e.clientY;
                }
            }, false);

            // 滚轮事件
            container.addEventListener("mousewheel", function (e) {
                hotbox.active("idle");
                // destroyRightMenu();
                // destroyEditText();
            }, false);

            // 屏蔽原生右键
            container.addEventListener("contextmenu", function (e) {
                e.preventDefault();
            });

            // 鼠标按键松开
            container.addEventListener("mouseup", function (e) {
                if (e.button != MOUSE_RB || e.clientX != downX || e.clientY != downY) {
                    return;
                }

                var node = minder.getSelectedNode();
                if (!node) {
                    return;
                }

                var box = node.getRenderBox();
                // createRightMenu(box.cx, box.cy);
                hotbox.active("main", { x: box.cx, y: box.cy });
            }, false);
            // 阻止热盒事件冒泡，在热盒正确执行前导致热盒关闭
            hotbox.$element.addEventListener("mousedown", function (e) {
                e.stopPropagation();
            });
        }
    });
});