define(function(require, exports) {
    var kity = require('./kity');
    var uuidMap = {};

    exports.extend = kity.Utils.extend.bind(kity.Utils);
    exports.each = kity.Utils.each.bind(kity.Utils);

    exports.uuid = function(group) {
        uuidMap[group] = uuidMap[group] ? uuidMap[group] + 1 : 1;
        return group + uuidMap[group];
    };

    exports.guid = function() {
        return (+new Date() * 1e6 + Math.floor(Math.random() * 1e6)).toString(36);
    };

    exports.trim = function(str) {
        return str.replace(/(^[ \t\n\r]+)|([ \t\n\r]+$)/g, '');
    };

    exports.keys = function(plain) {
        var keys = [];
        for (var key in plain) {
            if (plain.hasOwnProperty(key)) {
                keys.push(key);
            }
        }
        return keys;
    };

    exports.clone = function(source) {
        return JSON.parse(JSON.stringify(source));
    };

    exports.comparePlainObject = function(a, b) {
        return JSON.stringify(a) == JSON.stringify(b);
    };

    exports.encodeHtml = function(str, reg) {
        return str ? str.replace(reg || /[&<">'](?:(amp|lt|quot|gt|#39|nbsp);)?/g, function(a, b) {
            if (b) {
                return a;
            } else {
                return {
                    '<': '&lt;',
                    '&': '&amp;',
                    '"': '&quot;',
                    '>': '&gt;',
                    '\'': '&#39;'
                }[a];
            }
        }) : '';
    };

    exports.clearWhiteSpace = function(str) {
        return str.replace(/[\u200b\t\r\n]/g, '');
    };

    exports.each(['String', 'Function', 'Array', 'Number', 'RegExp', 'Object'], function(v) {
        var toString = Object.prototype.toString;
        exports['is' + v] = function(obj) {
            return toString.apply(obj) == '[object ' + v + ']';
        };
    });

    exports.getCustomNodeByNodeType = function(data) {
        // PF 
        // DF 界面17 子级18
        // 通用 根节点1 功能4 失效模式5 失效原因6 失效影响7 预防措施8 探测措施9 优化预防13 优化探测14 失效15 要求16
        var str;
        switch (data) {
            case 4:
                str = {"letter":"FU:", "color":"#228B22"};
                break;
            case 5:
                str = {"letter":"FM:","color":"#FF3030"};
                break;
            case 6:
                str = {"letter":"FC:","color":"#EE9A00"};
                break;
            case 7:
                str = {"letter":"FE:","color":"#EE7AE9"};
                break;
            case 8:
                str = {"letter":"PC:","color":"#EE9A00"};
                break;
            case 9:
                str = {"letter":"DC:","color":"#EE9A00"};
                break;
            case 10:
                str = {"letter":"OI:","color":"#4169E1"};
                break;
            case 11:
                str = {"letter":"II:","color":"#00008B"};
                break;
            case 12:
                str = {"color":"#3498db"};
                break;
            case 13:
                str = {"letter":"优:","color":"#EE9A00"};
                break;
            case 14:
                str = {"letter":"优:","color":"#EE9A00"};
                break;
            case 15:
                str = {"letter":"F:","color":"#FF3030"};
                break;
            case 16:
                str = {"letter":"R/C:","color":"#48B3FF"};
                break;
            case 17:
                str = {"color":"#3498db"};
                break;
            case 18:
                str = {};
                break;        
            default:
                str = {};
                break;
        }
        return str;
    };

    exports.getColorByLetter = function(data) {
        var str;
        switch (data) {
            case "FU:":
                str = "#228B22";
                break;
            case "FM:":
                str = "#FF3030";
                break;
            case "FC:":
                str = "#EE9A00";
                break;
            case "FE:":
                str = "#EE7AE9";
                break;
            case "PC:":
                str = "#EE9A00";
                break;
            case "DC:":
                str = "#EE9A00";
                break;
            case "OI:":
                str = "#4169E1";
                break;
            case "II:":
                str = "#00008B";
                break;
            case "优:":
                str = "#EE9A00";
                break;
            case "F:":
                str = "#FF3030";
                break;
            case "R/C:":
                str = "#48B3FF";
                break;
            default:
                str = null;
                break;
        }
        return str;
    };
});
