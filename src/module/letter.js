define(function(require, exports, module) {
    var kity = require('../core/kity');
    var utils = require('../core/utils');

    var Minder = require('../core/minder');
    var MinderNode = require('../core/node');
    var Command = require('../core/command');
    var Module = require('../core/module');
    var Renderer = require('../core/render');

    Module.register('LetterModule', function() {

        var LetterIcon = kity.createClass("LetterIcon", {
            base: kity.Group,

            constructor: function() {
                this.callBase();
                this.setSize(16);
                this.create();
                this.setId(utils.uuid('node_letter'));
            },

            setSize: function(size) {
                this.width = this.height = size;
            },

            create: function() {
                // var rect = new kity.Rect(this.width, this.height, -.5, 1, 4).stroke("#BEBEBE", 1)
                var text = new kity.Text()
                .setX(this.width / 2)
                .setY(this.height / 2)
                .setTextAnchor("middle")
                .setVerticalAlign("middle")
                .setFontSize(12)
                .fill("#3CB371");

                // this.addShape(rect);
                this.addShape(text);
                this.letter = text;
            },

            setValue: function(value) {
                var color = utils.getColorByLetter(value);
                
                if(null != color) {
                    this.letter.fill(color);
                }
                this.letter.setContent(value);
            }
        });
        
        var LetterCommand = kity.createClass('LetterCommand', {
            base: Command,
            execute: function(km) {
                km.getSelectedNode().setData("letter", "●").render();
                km.layout();
            }
        });

        return {
            'commands' : {
                'letter': LetterCommand
            },
            'renderers': {
                left: kity.createClass('LetterRenderer', {
                    base: Renderer,

                    create: function(node) {
                        return new LetterIcon();
                    },

                    shouldRender: function(node) {
                        return node.getData("letter");
                    },

                    update: function(icon, node, box) {
                        var data = node.getData("letter");
                        var spaceLeft = node.getStyle('space-left'),
                            x, y;

                        icon.setValue(data);
                        x = box.left - icon.width - spaceLeft;
                        y = -icon.height / 2;

                        icon.setTranslate(x, y);

                        return new kity.Box({
                            x: x,
                            y: y,
                            width: icon.width,
                            height: icon.height
                        });
                    }
                })
            }
        };
    });
});