define(function(require, exports, module) {
    var kity = require('../core/kity');
    var theme = require('../core/theme');

    function hsl(h, s, l) {
        return kity.Color.createHSL(h, s, l);
    }

    function generate(h, compat) {
        return {
            background: "#fbfbfb",
                "root-color": "white",
                "root-background": hsl(h, 37, 60),
                "root-stroke": hsl(h, 37, 60),
                "root-stroke-width": "1",
                "root-font-size": 16,
                "root-padding": [ 6, 12 ],
                "root-margin": 10,
                "root-radius": 5,
                "root-space": 10,

                "main-color": "black",
                "main-background": "white",
                "main-stroke": hsl(h, 37, 60),
                "main-stroke-width": 1,
                "main-font-size": 14,
                "main-padding": [ 5, 8 ],
                "main-margin": [4, 8],
                "main-radius": 3,
                "main-space": 5,

                "sub-color": "black",
                "sub-background": "transparent",
                "sub-stroke": "none",
                "sub-font-size": 12,
                "sub-padding": [ 3, 5 ],
                "sub-margin": [ 4, 8 ],
                "sub-radius": 5,
                "sub-space": 5,

                "connect-color": hsl(h, 37, 60),
                "connect-width": 1,
                "connect-radius": 5,

                "selected-stroke": hsl(h, 26, 30),
                "selected-stroke-width": "3",
                "blur-selected-stroke": hsl(h, 10, 60),

                "marquee-background": hsl(h, 100, 80).set("a", .1),
                "marquee-stroke": hsl(h, 37, 60),

                "drop-hint-color": hsl(h, 26, 35),
                "drop-hint-width": 5,

                "order-hint-area-color": hsl(h, 100, 30).set("a", .5),
                "order-hint-path-color": hsl(h, 100, 25),
                "order-hint-path-width": 1,

                "text-selection-color": hsl(h, 100, 20),
                "line-height": 1.5
        };
    }

    var plans = {
        red: 0,
        soil: 25,
        green: 122,
        blue: 204,
        purple: 246,
        pink: 334
    };
    var name;
    for (name in plans) {
        theme.register('custom-' + name, generate(204, true));
    }

});